using System.Reflection;
using System.Runtime.InteropServices;

// Note: Shared assembly information is specified in SharedAssemblyInfo.cs

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("EVEMon.MarketUnifiedUploader")]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("d40af584-9def-4d58-8bb3-4d2c288238fd")]
